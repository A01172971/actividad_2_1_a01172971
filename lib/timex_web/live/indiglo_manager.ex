defmodule TimexWeb.IndigloManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()
    {:ok, %{ui_pid: ui, st: Off, time: Time.from_erl!(now), alarm: Time.add(Time.from_erl!(now),6) }}
  end

  def handle_info(:"top-right", %{ui_pid: ui, st: Off} = state) do
    GenServer.cast(ui, :set_indiglo)
    {:noreply, state |> Map.put(:st, On)}
  end

  def handle_info(:"top-right", %{ui_pid: _ui, st: On} = state) do
    Process.send_after(self(), :"top-right", 2000)
    {:noreply, state |> Map.put(:st, TurnOff)}
  end

  def handle_info(:"top-right", %{ui_pid: ui, st: TurnOff} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, state |> Map.put(:st, Off)}
  end

  def handle_info(:"top-left", %{ui_pid: _ui, st: _Off} = state) do
    {:noreply, state }
  end

  def handle_info(:"bottom-left", %{ui_pid: _ui, st: _Off} = state) do
    {:noreply, state }
  end

  def handle_info(:"bottom-right", %{ui_pid: _ui, st: _Off} = state) do
    {:noreply, state }
  end

  def handle_info(:start_alarm, %{ui_pid: _ui, st: Off, time: time, alarm: _alarm} = state) do
    alarm = Time.add(time,5)
    {:noreply, %{state | st: AlarmOff, time: time, alarm: alarm} }
  end

  def handle_info(:start_alarm, %{ui_pid: ui, st: _, time: alarm, alarm: alarm }  = state) do
    alarm = Time.add(alarm, 1)
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, %{state | st: Off, time: alarm} }
  end

  def handle_info(:start_alarm, %{ui_pid: ui, st: AlarmOn, time: time, alarm: alarm }  = state) do
    Process.send_after(self(), :start_alarm, 1000)
    time = Time.add(time, 1)
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, %{state | st: AlarmOff, time: time, alarm: alarm} }
  end

  def handle_info(:start_alarm, %{ui_pid: ui, st: AlarmOff, time: time, alarm: alarm }  = state) do
    Process.send_after(self(), :start_alarm, 1000)
    time = Time.add(time, 1)
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: AlarmOn, time: time, alarm: alarm} }
  end

  def handle_info(:"top-right", %{ui_pid: _ui, st: AlarmOff, time: _, alarm: _} = state) do
    {:noreply, state}
  end
end
