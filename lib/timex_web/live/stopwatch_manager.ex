defmodule TimexWeb.StopwatchManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, count: ~T[00:00:00.00], st: Paused}}
  end

  def handle_info(:"bottom-right", %{st: Paused} = state) do
    Process.send_after(self(), :toCounting, 10)
    {:noreply, %{state | st: Counting}}
  end

  def handle_info(:"bottom-left", %{ui_pid: ui} = state) do
    {_, count} = Time.new(0, 0, 0, 0)
    GenServer.cast(ui, {:set_time_display, Time.to_string(count) |> String.slice(3, 8)})
    {:noreply, %{state | count: count}}
  end

  def handle_info(:toCounting, %{ui_pid: ui, st: Counting, count: count} = state) do
    Process.send_after(self(), :toCounting, 10)
    count = Time.add(count, 10, :millisecond)
    GenServer.cast(ui, {:set_time_display, Time.to_string(count) |> String.slice(3, 8)})
    {:noreply, %{state | count: count}}
  end

  def handle_info(:"bottom-right", %{st: Counting} = state) do
    {:noreply, %{state | st: Paused}}
  end

  def handle_info(_event, state) do
    {:noreply, state}
  end
end
