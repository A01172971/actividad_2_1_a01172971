defmodule TimexWeb.ClockManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()

    Process.send_after(self(), :working, 1000)
    {:ok, %{ui_pid: ui, time: Time.from_erl!(now), mode: Time, alarm: Time.add(Time.from_erl!(now),60), count: 0, selection: [:hour, :minute, :second], show: true}}
  end

  def handle_info(:working, %{ui_pid: _ui, time: alarm, mode: Time, alarm: alarm, count: _, selection: _, show: _} = state) do
    #GenServer.cast(ui, :start_alarm)
    :gproc.send({:p, :l, :ui_event}, :start_alarm)
    Process.send_after(self(), :working, 1000)
    alarm = Time.add(alarm, 1)
    #GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, state |> Map.put(:time, alarm) }
  end

  def handle_info(:working, %{ui_pid: ui, time: time} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, state |> Map.put(:time, time) }
  end

  def handle_info(_event, state), do: {:noreply, state}

  def handle_info(:working, %{ui_pid: _ui, time: time, mode: SWatch, alarm: _alarm, count: _, selection: _, show: _} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    {:noreply, state |> Map.put(:time, time) }
  end

  def handle_info(:start_alarm, %{ui_pid: _ui, time: _, mode: _, alarm: _alarm, count: _, selection: _, show: _} = state) do
    {:noreply, state}
  end

  def format(hour, selection, show) do
    if show == true do
      cond do
        List.first(selection) == :hour ->  "#{hour.hour}:#{hour.minute|> Integer.to_string()
          |> String.pad_leading(2,"0")}:#{hour.second
          |> Integer.to_string()
          |> String.pad_leading(2,"0")}"
        List.first(selection) == :minute ->  "#{hour.hour}:#{hour.minute}:#{hour.second
          |> Integer.to_string()
          |> String.pad_leading(2,"0")}"
        List.first(selection) == :second ->  "#{hour.hour}:#{hour.minute
          |> Integer.to_string()
          |> String.pad_leading(2,"0")}:#{hour.second
          |> Integer.to_string() |> String.pad_leading(2,"0")}"
        true -> "  "
      end
    else
      cond do
      List.first(selection) == :hour ->  "  :#{hour.minute|> Integer.to_string()
        |> String.pad_leading(2,"0")}:#{hour.second
        |> Integer.to_string()
        |> String.pad_leading(2,"0")}"
      List.first(selection) == :minute ->  "#{hour.hour}:  :#{hour.second
        |> Integer.to_string()
        |> String.pad_leading(2,"0")}"
      List.first(selection) == :second ->  "#{hour.hour}:#{hour.minute
        |> Integer.to_string()
        |> String.pad_leading(2,"0")}:  "
      true -> "  "
      end
    end
  end

  def change_selection(selection) do
    tl(selection) ++ [hd(selection)]
  end

  def increase_selection(time, selection) do
    cond do
      List.first(selection) == :hour -> Time.add(time, 3600)
        List.first(selection) == :minute -> Time.add(time, 60)
        List.first(selection) == :second ->  Time.add(time, 1)
        true -> time
    end
  end

  def handle_info(:"bottom-left", %{ui_pid: ui, time:  time, mode: TimeEdit, alarm: _alarm, count: _count, selection: selection, show: show} = state) do
    time = increase_selection(time, selection)
    GenServer.cast(ui, {:set_time_display, format(time, selection, true)})
    {:noreply, %{state | time: time, count: 0, show: true}}
  end

  def handle_info(:"bottom-right", %{ui_pid: ui, time:  time, mode: TimeEdit, alarm: _alarm, count: _count, selection: selection, show: true} = state) do
    selection = change_selection(selection)
    GenServer.cast(ui, {:set_time_display, format(time, selection, true)})
    {:noreply, %{state | count: 0,  selection: selection, show: true}}
  end

  def handle_info(:editing, %{ui_pid: ui, time: time, mode: TimeEdit, alarm: _alarm, count: count, selection: selection, show: show} = state) do
    Process.send_after(self(), :editing, 250)
    cond do
      count < 20 -> GenServer.cast(ui, {:set_time_display, format(time, selection, !show)})
      {:noreply, %{state | count: count+1, selection: selection, show: !show}}
      count == 20 -> GenServer.cast(ui, {:set_time_display, format(time, selection, true)})
      Process.send_after(self(), :working, 0)
      {:noreply, %{state | mode: Time}}
    end
  end

  def handle_info(:"bottom-right", %{alarm: _, count: _, mode: TimeEdit, selection: _, show: false, time: _, ui_pid: _} = state)do
    {:noreply, state}
  end

  def handle_info(:waiting, %{ui_pid: ui, time: time, mode: TimeEdit, alarm: _alarm, count: _, selection: selection, show: true} = state) do
    Process.send_after(self(), :editing, 0)
    {:noreply, state}
  end

  def handle_info(:waiting, %{ui_pid: ui, time: time, mode: TimeEdit, alarm: _alarm, count: _, selection: selection, show: false} = state) do
    {:noreply, state}
  end

  def handle_info(:waiting, %{ui_pid: ui, time: time, mode: TimeEdit, alarm: _alarm, count: _, selection: selection, show: false} = state) do
    {:noreply, state}
  end

  def handle_info(:"bottom-right", %{ui_pid: _ui, time: _time, mode: Time, alarm: _alarm, count: _, selection: _, show: _} = state) do
    Process.send_after(self(), :waiting, 250)
    {:noreply, state |> Map.put(:mode, TimeEdit) }
  end

  def handle_info(:"bottom-left", %{ui_pid: _ui, time:  _time, mode: Time, alarm: _alarm, count: _count, selection: _selection, show: _show} = state) do
    Process.send_after(self(), :editing, 250)
    #GenServer.cast(ui, {:set_time_display, format(time, selection, true)})
    {:noreply, %{state | mode: AlarmEdit, count: 0, show: true}}
  end

  def handle_info(:"bottom-left", %{ui_pid: ui, time:  _time, mode: AlarmEdit, alarm: alarm, count: _count, selection: selection, show: _show} = state) do
    alarm = increase_selection(alarm, selection)
    GenServer.cast(ui, {:set_time_display, format(alarm, selection, true)})
    #GenServer.cast(ui, {:set_time_display, format(time, selection, true)})
    {:noreply, %{state | mode: AlarmEdit, alarm: alarm, count: 0, show: true}}
  end

  def handle_info(:"bottom-right", %{ui_pid: ui, time:  time, mode: AlarmEdit, alarm: alarm, count: _count, selection: selection, show: _show} = state) do
    selection = change_selection(selection)
    GenServer.cast(ui, {:set_time_display, format(alarm, selection, true)})
    {:noreply, %{state | time: time, count: 0, selection: selection, show: true}}
  end

  def handle_info(:editing, %{ui_pid: ui, time: time, mode: AlarmEdit, alarm: alarm, count: count, selection: selection, show: show} = state) do
    Process.send_after(self(), :editing, 250)
    cond do
      count < 20 -> GenServer.cast(ui, {:set_time_display, format(alarm, selection, !show)})
      {:noreply, %{state | count: count+1, selection: selection, show: !show}}
      count == 20 -> GenServer.cast(ui, {:set_time_display, format(alarm, selection, true)})
      Process.send_after(self(), :working, 0)
      {:noreply, %{state | mode: Time, alarm: alarm}}
    end
  end

  def handle_info(:waiting, %{ui_pid: ui, time: time, mode: AlarmEdit, alarm: _alarm, count: _, selection: selection, show: true} = state) do
    {:noreply, state}
  end

  def handle_info(:working, %{alarm: _, count: _, mode: AlarmEdit, selection: _, show: false, time: _, ui_pid: _} = state) do
    {:noreply, state}
  end
end
